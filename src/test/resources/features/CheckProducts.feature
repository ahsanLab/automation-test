@CheckProducts
Feature: Checking items
  As a user, I want to able to see the item details

  Background:
    Given user is already on home page

  @PositiveCase @CheckDetail @DetailBackpack
  Scenario: user check backpack detail
    When user clicks on the backpack label
    Then user will be navigates to the backpack detail
      | productName         | price |
      | Sauce Labs Backpack | $29.99 |

  @PositiveCase @CheckDetail @DetailBaikeLight
  Scenario: user check bike light detail
    When user clicks on the bike light label
    Then user will be navigates to the bike light detail
      | productName           | price |
      | Sauce Labs Bike Light | $9.99 |

  @PositiveCase @CheckDetail @DetailBoltTShirt
  Scenario: user check bolt t-shirt detail
    When user clicks on the bolt t-shirt label
    Then user will be navigates to the bolt t-shirt detail
      | productName             | price  |
      | Sauce Labs Bolt T-Shirt | $15.99 |

