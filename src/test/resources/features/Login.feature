Feature: Login Feature
  As a user, I want to able to login into the application so that I can enjoy all the existing features

  Background:
    Given user navigates to the login page

  @login @ValidCredentials
  Scenario: Login with valid credentials
    And user enters valid username
    And user enters valid password
    When user clicks login button
    Then user should be navigated to home page

  @login @InvalidCredentials
  Scenario: Login with invalid credentials
    And user enters invalid username
    And user enters invalid password
    When user clicks login button
    Then user will be get error message invalid credentials



