@Logout
Feature: Logout Feature
  As a user, I want to able to logout the application

  @PositiveCase @Iteration
  Scenario: Logout some account
    Given user is already on home page
    And user clicks menu icon
    When user click logout menu
    Then user logout