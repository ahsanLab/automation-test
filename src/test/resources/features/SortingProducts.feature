@SortingProduct
Feature: Sorting the items
  As a user, I want to able to sorting the items

  Background:
    Given user is already on home page
    And user clicks on sorting icon

  @PositiveCase @Sorting @SortingByAlphabet
  Scenario: user sorting by alphabet Z to A
    When user selects the alphabet Z to A
    Then item ordered by alphabet Z to A

  @PositiveCase @Sorting @SortingByAlphabet
  Scenario: user sorting by alphabet A to Z
    When user selects the alphabet A to Z
    Then item ordered by alphabet A to Z

  @PositiveCase @Sorting @SortingByAlphabet
  Scenario: user sorting by price low to high
    When user selects the price low to high
    Then item ordered by price low to high

  @PositiveCase @Sorting @SortingByAlphabet
  Scenario: user sorting by price high to low
    When user selects the price high to low
    Then item ordered by price high to low
