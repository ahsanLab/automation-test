@CheckOutMultipleProduct
Feature: Checkout multiple item
  As a user, I want to able to check item details, select the item and to buy multiple item

  Background:
    Given user is already on home page

  @PositiveCase @OrderBackpack @OrderBikeLight @MultipleOrder
  Scenario: user buy the multiple item, the item is a backpack and bike light
    And user click add to cart button on the backpack
    And user click add to cart button on the bike light
    And user open cart detail
    And user clicks the checkout button
    When user confirms the first name <firstName>, last name <lastName> and postal code <postalCode>
      | firstName | lastName | postalCode |
      | Randy     | Blythe   | 12309      |
    And user clicks the continue button
    And user clicks finish button
    Then items successfully ordered

  @PositiveCase @OrderTShirt @OrderJacket @MultipleOrder
  Scenario: user buy the multiple item, the item is a t-shirt and jacket
    And user click add to cart button on the t-shirt
    And user click add to cart button on the jacket
    And user open cart detail
    And user clicks the checkout button
    When user confirms the first name <firstName>, last name <lastName> and postal code <postalCode>
      | firstName | lastName | postalCode |
      | Randy     | Blythe   | 12309      |
    And user clicks the continue button
    And user clicks finish button
    Then items successfully ordered

