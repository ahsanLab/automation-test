@CheckOutProduct
Feature: Checkout single item
  As a user, I want to able to check item details, select the item and to buy a single item

  Background:
    Given user is already on home page

  @OrderBackpack @SingleOrder @current
  Scenario: user buy the single item, the item is a backpack
    And user click add to cart button on the backpack
    And user open cart detail
    And user clicks the checkout button
    When user confirms the first name <firstName>, last name <lastName> and postal code <postalCode>
      | firstName | lastName | postalCode |
      | Randy     | Blythe   | 12309      |
    And user clicks the continue button
    And user clicks finish button
    Then items successfully ordered

  @OrderBikeLight @SingleOrder
  Scenario: user buy the single item, the item is a bike light
    And user clicks the checkout button on the bike light
    When user confirms the first name <firstName>, last name <lastName> and postal code <postalCode>
      | firstName | lastName | postalCode |
      | Hayley    | Williams | 35632      |
    And user clicks the continue button
    And user clicks finish button
    Then items successfully ordered

  @OrderTShirt @SingleOrder
  Scenario: user buy the single item, the item is a t-shirt
    And user clicks the checkout button on the t-shirt
    When user confirms the first name <firstName>, last name <lastName> and postal code <postalCode>
      | firstName | lastName | postalCode |
      | James     | Alan     | 11425      |
    And user clicks the continue button
    And user clicks finish button
    Then items successfully ordered
