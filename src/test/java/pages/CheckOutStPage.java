package pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckOutStPage extends PageObject {
    @FindBy(id = "first-name")
    public WebElement firstName;

    @FindBy(id = "last-name")
    public WebElement lastName;

    @FindBy(id = "postal-code")
    public WebElement postalCode;

    @FindBy(id = "cancel")
    public WebElement cancelBtn;

    @FindBy(id = "continue")
    public WebElement continueBtn;

    public void inputFirstName(String fName) {
        typeInto(firstName, fName);
    }

    public void inputLastName(String lName) {
        typeInto(lastName, lName);
    }

    public void inputPostalCode(String posCode) {
        typeInto(postalCode, posCode);
    }

    public void clickOnCancel() {
        clickOn(cancelBtn);
    }

    public void clickOnContinue() {
        clickOn(continueBtn);
    }
}
