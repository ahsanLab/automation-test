package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class HomePage extends PageObject {


    @FindBy(id ="shopping_cart_container")
    WebElementFacade shoppingCart;


    public boolean isOnHomePage(){
        return shoppingCart.isDisplayed();
    }
}
