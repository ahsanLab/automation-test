package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utilities.BaseTest;

public class LoginPage extends PageObject {
    public final String SAUCE_LAB = BaseTest.getBaseUrl();


    @FindBy(id = "user-name")
    WebElementFacade userName;

    @FindBy(id = "password")
    WebElementFacade pass;

    @FindBy(id = "login-button")
    WebElementFacade loginBtn;

    @FindBy(xpath = "//*[@id=\"login_button_container\"]/div/form/div[3]/h3")
    WebElementFacade errorMsgLb;

    @FindBy(className = "svg[data-icon='times']")
    WebElementFacade closeErrorMsgBtn;

    @FindBy(xpath = "//*[@id=\"root\"]/div/div[2]/div[1]/div[2]")
    WebElementFacade sauceLabBigImg;

    public void openSauceLabWeb(){
        openUrl(SAUCE_LAB);

    }
    public void entersUser() {
        waitFor(ExpectedConditions.urlToBe(SAUCE_LAB));
        typeInto(userName, BaseTest.getValidUser());
    }

    public void entersPassword() {
        typeInto(pass, BaseTest.getValidPassword());
    }

    public void clearName() {
        userName.clear();
    }

    public void clearPass() {
        pass.clear();
    }

    public void clickOnLogin() {
        clickOn(loginBtn);
    }

    public void closeErrorMessage() {
        clickOn(closeErrorMsgBtn);
    }

    public String getErrorMessage() {
        waitFor(errorMsgLb);
        return errorMsgLb.getText();

    }

    public boolean isInLoginPage() {
        return sauceLabBigImg.isDisplayed();
    }

    public void entersInvalidUser() {
        waitFor(ExpectedConditions.urlToBe(SAUCE_LAB));
        typeInto(userName, BaseTest.getInValidUser());
    }

    public void entersInvalidPass() {
        typeInto(pass, BaseTest.getInValidPassword());
    }
}
