package pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class InventoryPage extends PageObject {
    @FindBy(id = "react-burger-menu-btn")
    public WebElement menuIcon;

    @FindBy(xpath = "//*[@id=\"logout_sidebar_link\"]")
    public WebElement logoutSubMenu;

    @FindBy(xpath = "//*[@id=\"react-burger-cross-btn\"]")
    public WebElement closeMenu;

    @FindBy(xpath = "//*[@id=\"inventory_sidebar_link\"]")
    public WebElement allItemSubMenu;

    public void clickOnMenu() {
        clickOn(menuIcon);
    }

    public void clickOnLogout() {
        clickOn(logoutSubMenu);

    }

    public void clickOnAllItem() {
        clickOn(allItemSubMenu);
    }

    public void closeMenu() {
        clickOn(closeMenu);
    }

    @FindBy(id = "add-to-cart-sauce-labs-backpack")
    public WebElement backPackAddCartBtn;

    @FindBy(id = "add-to-cart-sauce-labs-bike-light")
    public WebElement bikeLightAddCartBtn;

    @FindBy(id = "add-to-cart-sauce-labs-bolt-t-shirt")
    public WebElement tShirtAddCartBtn;

    @FindBy(id = "add-to-cart-sauce-labs-fleece-jacket")
    public WebElement fleeceJacketAddCartBtn;


    @FindBy(id = "item_4_title_link")
    public WebElement sauceLabsBackpack;

    @FindBy(id = "item_0_title_link")
    public WebElement sauceLabsBikeLight;

    @FindBy(id = "item_1_title_link")
    public WebElement sauceLabsBoltTShirt;

    @FindBy(id = "item_5_title_link")
    public WebElement sauceLabsFleeceJacket;

    @FindBy(id = "item_2_title_link")
    public WebElement sauceLabsOnesie;

    @FindBy(id = "item_3_title_link")
    public WebElement sauceLabsRedTShirt;

    public void selectBlackBackPack() {
        clickOn(backPackAddCartBtn);
    }

    public void selectBikeLight() {
        clickOn(bikeLightAddCartBtn);
    }

    public void selectBlackTShirt() {
        clickOn(tShirtAddCartBtn);
    }

    public void selectFleeceJacket() {
        clickOn(fleeceJacketAddCartBtn);
    }

    public void clickOnBackpackLabel() {
        clickOn(sauceLabsBackpack);
    }

    public void clickOnBikeLightLabel() {
        clickOn(sauceLabsBikeLight);
    }

    public void clickOnBlackTShirtLabel() {
        clickOn(sauceLabsBoltTShirt);
    }

    public void clickOnFleeceJacketLabel() {
        clickOn(sauceLabsFleeceJacket);
    }
    public void clickOnOnesieLabel() {
        clickOn(sauceLabsOnesie);
    }
    public void clickOnRedTShirtLabel() {
        clickOn(sauceLabsRedTShirt);
    }


    @FindBy(id = "shopping_cart_container")
    public WebElement shoppingContainerIcon;

    public void clickOnShippingContainer() {
        clickOn(shoppingContainerIcon);
    }

    @FindBy(xpath = "//*[@id=\"header_container\"]/div[2]/div[2]/span/select")
    public WebElement sortIcon;

    @FindBy(xpath = "//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[1]")
    public WebElement aToZ;

    @FindBy(xpath = "//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[2]")
    public WebElement zToA;

    @FindBy(xpath = "//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[3]")
    public WebElement lowHigh;

    @FindBy(xpath = "//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[4]")
    public WebElement highLow;

    public void clickOnSortIcon() {
        clickOn(sortIcon);
    }

    public void sortAlphaAtoZ() {
        clickOn(aToZ);
    }

    public void sortAlphaZToA() {
        clickOn(zToA);
    }

    public void sortPriceLowHigh() {
        clickOn(lowHigh);
    }

    public void sortPriceHighLow() {
        clickOn(highLow);
    }


}
