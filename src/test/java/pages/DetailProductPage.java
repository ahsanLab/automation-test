package pages;

import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DetailProductPage extends PageObject {
    //@FindBy(xpath = "//*[@id=\"inventory_item_container\"]/div/div/div[2]/div[1]")
    @FindBy(css = "#inventory_item_container > div > div > div.inventory_details_desc_container > div.inventory_details_name.large_size")
    public WebElement productTitle;

    //@FindBy(xpath = "//*[@id=\"inventory_item_container\"]/div/div/div[2]/div[3]/text()[2]")
    @FindBy(css = "#inventory_item_container > div > div > div.inventory_details_desc_container > div.inventory_details_price")
    public WebElement productPrice;

    @FindBy(css = "#inventory_item_container > div > div > div.inventory_details_desc_container > div.inventory_details_desc.large_size")
    public WebElement productDescription;

    @FindBy(id = "back-to-products")
    public WebElement backBtn;

    public void clickOnBackButton() {
        clickOn(backBtn);
    }

    public void verifyProductTitle(String prdTitle) {
        boolean prdTtl = productTitle.isDisplayed();
        String prdTtlActual = productTitle.getText();
        System.out.println("Title -> "+prdTtlActual);
        Assert.assertEquals(true, prdTtl);
        Assert.assertEquals(prdTitle, prdTtlActual);
    }

    public void verifyProductPrice(String prdPrice) {
        boolean prdPrc = productPrice.isDisplayed();
        String prdPrcActual = productPrice.getText();
        System.out.println("Title -> "+prdPrcActual);
        Assert.assertEquals(true, prdPrc);
        Assert.assertEquals(prdPrice, prdPrcActual);
    }

}
