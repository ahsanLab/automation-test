package pages;

import com.fasterxml.jackson.databind.jsontype.impl.AsExistingPropertyTypeSerializer;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckOutCompletedPage extends PageObject {
    public String header = "THANK YOU FOR YOUR ORDER";
    public String content = "Your order has been dispatched, and will arrive just as fast as the pony can get there!";
    @FindBy(id = "back-to-products")
    public WebElement backToHomeBtn;

    @FindBy (xpath = "//*[@id=\"checkout_complete_container\"]/h2") // THANK YOU FOR YOUR ORDER
    public WebElement headerText;

    @FindBy (xpath = "//*[@id=\"checkout_complete_container\"]/div") // Your order has been dispatched, and will arrive just as fast as the pony can get there!
    public WebElement successOrderText;

    @FindBy (xpath = "//*[@id=\"checkout_complete_container\"]/img")
    public WebElement ponyExpressImg;

    public void clickOnBackToHome(){
        clickOn(backToHomeBtn);
    }
    public void isInCheckoutCompleted(){
        String hdr = headerText.getText();
        System.out.println(hdr);
        Assert.assertEquals(header, hdr);
        String cnt = successOrderText.getText();
        System.out.println(cnt);
        Assert.assertEquals(content, cnt);
    }
}
