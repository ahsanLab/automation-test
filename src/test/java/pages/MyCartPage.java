package pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyCartPage extends PageObject {
    @FindBy (id = "continue-shopping")
    public WebElement continueShoppingBtn;

    @FindBy (id = "checkout")
    public WebElement checkOutBtn;

    @FindBy (id = "remove-sauce-labs-backpack")
    public WebElement removeBackPackBtn;

    @FindBy (id = "remove-sauce-labs-bike-light")
    public WebElement removeBikeLightBtn;

    @FindBy (id = "remove-sauce-labs-bolt-t-shirt")
    public WebElement removeTShirtBtn;

    @FindBy (id = "remove-sauce-labs-fleece-jacket")
    public WebElement removeFleeceJacketBtn;

    public void removeBackPack(){
        clickOn(removeBackPackBtn);
    }
    public void removeBikeLight(){
        clickOn(removeBikeLightBtn);
    }
    public void removeTShirt(){
        clickOn(removeTShirtBtn);
    }
    public void removeFleece(){
        clickOn(removeFleeceJacketBtn);
    }
    public void clickOnCheckOut(){
        clickOn(checkOutBtn);
    }
    public void clickOnContinueShopping(){
        clickOn(continueShoppingBtn);
    }
}
