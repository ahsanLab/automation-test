package pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckOutNdPage extends PageObject {
    @FindBy (id = "cancel")
    public WebElement cancelBtn;

    @FindBy (id = "finish")
    public WebElement finishBtn;

    @FindBy (xpath = "//*[@id=\"checkout_summary_container\"]/div/div[2]/div[5]/text()[2]")
    public WebElement itemTotal;

    @FindBy (xpath = "//*[@id=\"checkout_summary_container\"]/div/div[2]/div[6]/text()[2]")
    public WebElement tax;

    @FindBy (xpath = "//*[@id=\"checkout_summary_container\"]/div/div[2]/div[7]/text()[2]")
    public WebElement total;

    public void clickOnCancel(){
        clickOn(cancelBtn);
    }
    public void clickOnFinish(){
        clickOn(finishBtn);
    }
//    public void checkTotal(){
//
//    }
}
