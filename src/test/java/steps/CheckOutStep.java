package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import pages.*;

import java.util.List;
import java.util.Map;

public class CheckOutStep {
    @Steps
    LoginPage login;
    @Steps
    InventoryPage inv;
    @Steps
    MyCartPage mycart;
    @Steps
    CheckOutStPage firstCheckout;
    @Steps
    CheckOutNdPage secondCheckout;
    @Steps
    CheckOutCompletedPage lastCheckout;

    @And("user click add to cart button on the backpack")
    public void userClickAddToCartButtonOnTheBackpack() {
    }

    @And("user open cart detail")
    public void userOpenCartDetail() {
    }

    @And("user clicks the checkout button")
    public void userClicksTheCheckoutButton() {
    }

    @When("user confirms the first name {}, last name {} and postal code {}")
    public void userConfirmsTheFirstNameLastNameAndPostalCode(String arg0, String arg1, String arg2) {
    }

    @And("user clicks the continue button")
    public void userClicksTheContinueButton() {
    }

    @And("user clicks finish button")
    public void userClicksFinishButton() {
    }

    @Then("items successfully ordered")
    public void itemsSuccessfullyOrdered() {
    }

    @And("user clicks the checkout button on the bike light")
    public void userClicksTheCheckoutButtonOnTheBikeLight() {
    }

    @And("user clicks the checkout button on the t-shirt")
    public void userClicksTheCheckoutButtonOnTheTShirt() {
    }

}
