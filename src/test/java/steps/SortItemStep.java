package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import pages.*;

import java.util.List;
import java.util.Map;

public class SortItemStep {
    @Steps
    LoginPage login;
    @Steps
    InventoryPage inv;

    @And("user clicks on sorting icon")
    public void userClicksOnSortingIcon() {
    }

    @When("user selects the alphabet Z to A")
    public void userSelectsTheAlphabetZToA() {
    }

    @Then("item ordered by alphabet Z to A")
    public void itemOrderedByAlphabetZToA() {
    }

    @When("user selects the alphabet A to Z")
    public void userSelectsTheAlphabetAToZ() {
    }

    @Then("item ordered by alphabet A to Z")
    public void itemOrderedByAlphabetAToZ() {
    }

    @When("user selects the price low to high")
    public void userSelectsThePriceLowToHigh() {
    }

    @Then("item ordered by price low to high")
    public void itemOrderedByPriceLowToHigh() {
    }

    @When("user selects the price high to low")
    public void userSelectsThePriceHighToLow() {
    }

    @Then("item ordered by price high to low")
    public void itemOrderedByPriceHighToLow() {
    }

}
