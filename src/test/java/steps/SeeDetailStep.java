package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import pages.*;

import java.util.List;
import java.util.Map;

public class SeeDetailStep {
    @Steps
    LoginPage login;
    @Steps
    InventoryPage inv;

    @Steps
    DetailProductPage dtlProduct;

    @When("user clicks on the backpack label")
    public void userClicksOnTheBackpackLabel() {
    }

    @Then("user will be navigates to the backpack detail")
    public void userWillBeNavigatesToTheBackpackDetail() {
    }

    @When("user clicks on the bike light label")
    public void userClicksOnTheBikeLightLabel() {
    }

    @Then("user will be navigates to the bike light detail")
    public void userWillBeNavigatesToTheBikeLightDetail() {
    }

    @When("user clicks on the bolt t-shirt label")
    public void userClicksOnTheBoltTShirtLabel() {
    }

    @Then("user will be navigates to the bolt t-shirt detail")
    public void userWillBeNavigatesToTheBoltTShirtDetail() {
    }

}
