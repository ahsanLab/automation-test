package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import pages.*;

import java.util.List;
import java.util.Map;

public class CheckOutMultipleItemStep {
    @Steps
    LoginPage login;
    @Steps
    InventoryPage inv;
    @Steps
    MyCartPage mycart;
    @Steps
    CheckOutStPage firstCheckout;
    @Steps
    CheckOutNdPage secondCheckout;
    @Steps
    CheckOutCompletedPage lastCheckout;

    @And("user click add to cart button on the bike light")
    public void userClickAddToCartButtonOnTheBikeLight() {
    }

    @And("user click add to cart button on the t-shirt")
    public void userClickAddToCartButtonOnTheTShirt() {
    }

    @And("user click add to cart button on the jacket")
    public void userClickAddToCartButtonOnTheJacket() {
    }
}
