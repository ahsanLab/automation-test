package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.Data;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import pages.HomePage;
import pages.LoginPage;

import java.util.List;
import java.util.Map;

public class LoginStep {
    @Steps
    LoginPage login;
    HomePage homePage;

    @Given("user navigates to the login page")
    public void userNavigatesToTheLoginPage() {
        login.openSauceLabWeb();
    }

    @And("user enters valid username")
    public void userEntersValidUsername() {
        login.entersUser();
    }

    @And("user enters valid password")
    public void userEntersValidPassword() {
        login.entersPassword();
    }

    @When("user clicks login button")
    public void userClicksLoginButton() {
        login.clickOnLogin();
    }

    @Then("user should be navigated to home page")
    public void userShouldBeNavigatedToHomePage() {
        Assert.assertTrue(homePage.isOnHomePage());
    }

    @And("user enters invalid username")
    public void userEntersInvalidUsername() {
        login.entersInvalidUser();
    }

    @And("user enters invalid password")
    public void userEntersInvalidPassword() {
        login.entersInvalidPass();
    }

    @Then("user will be get error message invalid credentials")
    public void userWillBeGetErrorMessageInvalidCredentials() {
        Assert.assertEquals(Data.errorMessageLogin(), login.getErrorMessage());
    }

}
