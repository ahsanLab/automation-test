package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import pages.InventoryPage;
import pages.LoginPage;

public class LogoutStep {

    @Steps
    LoginPage login;

    @Steps
    InventoryPage ivn;

    @And("user clicks menu icon")
    public void userClicksMenuIcon() {
    }

    @When("user click logout menu")
    public void userClickLogoutMenu() {
    }

    @Then("user logout")
    public void userLogout() {
    }
}
