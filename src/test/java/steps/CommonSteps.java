package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.Data;
import net.thucydides.core.annotations.Steps;
import pages.HomePage;
import pages.LoginPage;

public class CommonSteps {
    @Steps
    HomePage homePage;
    LoginPage loginPage;

    @Given("user is already on home page")
    public void userIsAlreadyOnHomePage() {
        loginPage.openSauceLabWeb();
        loginPage.entersUser();
        loginPage.entersPassword();
        loginPage.clickOnLogin();

    }


}
